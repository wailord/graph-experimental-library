try:
    from gel._version import __version__, __version_tuple__
except ImportError:
    from setuptools_scm import get_version

    __version__ = get_version(root="..", relative_to=__file__)
    __version_tuple__ = tuple(int(part) if part.isdigit() else part for part in __version__.split("."))


__all__ = ["__version__", "__version_tuple__"]
