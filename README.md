# 1. Setting up project, environment and dev tools.

## Setting up Python and Installing Requirements

Follow these steps to set up Python and install the necessary requirements:

1. Ensure you have Python 3.12 installed. You can download it from the [official Python website](https://www.python.org/downloads/).

2. Install `virtualenvwrapper`. This tool provides a straightforward way to create virtual environments for Python. You can install it globally using pip:

```bash
pip install virtualenvwrapper
```

3. Create a new virtual environment using `virtualenvwrapper`. Replace `myenv` with the name you want to give to your virtual environment:

```bash
mkvirtualenv --python=/usr/bin/python3.12 myenv
```

4. Once the virtual environment is created, it should be activated automatically. If not, you can activate it using the `workon` command:

```bash
workon myenv
```

5. Now, navigate to the root directory of this project in your terminal. Install the project requirements using pip. The requirements are listed in the `requirements.txt` and `requirements-dev.txt` files:

```bash
pip install -r requirements.txt
pip install -r requirements-dev.txt
pip install -r requirements-test.txt
```

Now, you have a Python 3.12 virtual environment set up with all the necessary requirements installed.

## Setting up Pre-commit Hooks

This project uses pre-commit hooks to ensure code quality and consistency. Before you can commit your changes, pre-commit will run checks using tools like `black`, `flake8`, `isort`, `mypy`, `codespell`, and `pydocstyle`.

Follow these steps to set up pre-commit hooks:

1. Ensure you have Python 3.10 installed. You can download it from the [official Python website](https://www.python.org/downloads/).

2. Install pre-commit. You can do this by adding it to your project's dependencies or installing it globally using pip:

```bash
pip install pre-commit
```
3. Install the git hooks:
Navigate to the root directory of this project in your terminal and run this command:

```bash
pre-commit install
```
Now, every time you commit your changes, the hooks defined in the .pre-commit-config.yaml file will be executed. If any of these hooks fail, the commit will be aborted.


# Running Tests

This project uses pytest for testing. Follow these steps to run the tests:

1. Ensure you have pytest installed. If not, you can install it using pip:

```bash
pip install -r requirements-test.txt
```

2. Navigate to the root directory of this project in your terminal.

3. Run the tests using the following command:

```bash
pytest
```

Pytest will automatically discover and run all tests in your project. If a test fails, pytest will provide detailed output about the failure.

Remember to always run the tests before committing your changes to ensure that your code is working as expected.
